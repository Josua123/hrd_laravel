@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($lowongan as $lowongans)
            <div class="card mb-3">
                <div class="card-body">
                    <h3 class="card-title">{{$lowongans->namaPekerjaan}}</h3>
                    <h6 class="card-text">{{$lowongans->bidang_pekerjaan}}</h6>
                    <h6 class="card-text">Pendidikan: {{$lowongans->tingkat_pendidikan}}</h6>
                    <h6 class="card-text">{{$lowongans->kualifikasiPekerjaan}}</h6>
                    <h6 class="card-text">{{$lowongans->deskripsiPekerjaan}}</h6>
                    <p class="card-text">Exp: {{$lowongans->tanggal_expired}}</p>
                    <a href="{{Route('lowongan.lamar',$lowongans->id)}}" class="btn btn-info mt-5">Lamar Pekerjaan Ini</a>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
