@extends('layouts.user.app')

@section('content')
<div class="container">
      <div class="card mb-3 mb-5">
          <div class="card-body">
            <form class="" action="/formlaraman/submit" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              @foreach($lowongan as $lowongans)
              <input type="hidden" name="id_lowongan" value="{{$lowongans->id}}">
              <h2>Lamar sebagai {{$lowongans->namaPekerjaan}}</h2>
              <div class="card-body mb-5" style="background:lightgray;">
                <table>
                  <tr>
                    <td>
                      <h5>Jabatan yang dilamar </h5>
                    </td>
                    <td> <h5>:</h5> </td>
                    <td >
                      <h5 style="margin-right:350px;"> {{$lowongans->namaPekerjaan}}</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Pendidikan Yang dibutuhkan </h5>
                    </td>
                    <td> <h5>:</h5> </td>
                    <td >
                      <h5 style="margin-right:350px;"> {{$lowongans->tingkat_pendidikan}}</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Tersedia sampai </h5>
                    </td>
                    <td> <h5>:</h5> </td>
                    <td >
                      <h5 style="margin-right:350px;"> {{$lowongans->tanggal_expired}}</h5>
                    </td>
                  </tr>
                </table>
            </div>
            @endforeach
              <!-- data pendidikan -->
              <div class="card-body">
                <h4 style="font-weight:bold;">Data Pendidikan</h4>
                <hr>
                <div class="row mb-3">
                  <div style="font-weight:bold;" class="col-md-6">Nama Sekolah</div>
                  <div style="font-weight:bold;" class="col-md-2">Tahun Pendidikan</div>
                  <div style="font-weight:bold;" class="col-md-2">TingkatPendidikan</div>
                  <div style="font-weight:bold;" class="col-md-2">Jurusan</div>
                </div>
                @foreach($pendidikans as $pendidikan)
                <?php
                  $tingkats = App\TingkatPendidikan::where('id',$pendidikan->Tingkat_pendidikan_idTingkat_pendidikan)->get();
                ?>
                  @foreach($tingkats as $tingkat)
                    <?php
                      $details = App\DetailPendidikan::where('id',$tingkat->detail_tingkatpendidikan_id)->get();
                    ?>
                    @foreach($details as $detail)
                    <div class="row mb-3">
                      <div class="col-md-6">{{$detail->nama_sekolah}}</div>
                      <div class="col-md-2">{{$detail->tahun_pendidikan}}</div>
                      <div class="col-md-2">{{$tingkat->tingkat_pendidikan}}</div>
                      <div class="col-md-2">{{$detail->jurusan}}</div>
                    </div>
                    @endforeach
                  @endforeach
                @endforeach
                <hr>
                <!-- data pengalaman -->

                @foreach($pengalamans as $pengalaman)
                  <h4 style="font-weight:bold;">Data Pengalaman</h4>
                  <hr>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Nama Perusahaan :</div>
                    <div class="col-md">{{$pengalaman->nama_perusahaan}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Nama Atasan :</div>
                    <div class="col-md">{{$pengalaman->nama_atasan}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Rentang Waktu</div>
                    <div class="col-md">{{$pengalaman->rentang_waktu}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Gaji Pokok</div>
                    <div class="col-md">{{$pengalaman->nama_perusahaan}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Posisi Terakhir</div>
                    <div class="col-md">{{$pengalaman->posisi_terakhir}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Alasan Keluar</div>
                    <div class="col-md">{{$pengalaman->alasan_keluar}}</div>
                  </div>
                  <div class="row mb-2">
                    <div style="font-weight:bold;" class="col-md-2">Uraian Pekerjaan</div>
                    <div class="col-md">{{$pengalaman->uraian_pekerjaan}}</div>
                  </div>
                  <hr>
                @endforeach
              </div>
                <div class="row mb-3">
                  <div class="col-md-3"></div>
                  <div class="col-md-3"></div>
                  <div class="col-md-3"></div>
                  <button class="btn btn-primary col-md-2 ml-5" type="submit" name="button" style="height:60px;">Lamar</button>
                </div>
            </form>
      </div>
    </div>
</div>
@endsection
