@extends('layouts.user.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($lowongan as $lowongans)
            <div class="card mb-3">
              <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <h3 class="card-title">{{$lowongans->namaPekerjaan}}</h3>
                    <h6 class="card-text">{{$lowongans->bidang_pekerjaan}}</h6>
                    <h6 class="card-text">Pendidikan: {{$lowongans->tingkat_pendidikan}}</h6>
                    <h6 class="card-text">{{$lowongans->kualifikasiPekerjaan}}</h6>
                    <a href="{{Route('lowongan.detail',$lowongans->id)}}" class="btn btn-success mb-3">Details</a>
                    <p class="card-text">Expired at: {{$lowongans->tanggal_expired}}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
