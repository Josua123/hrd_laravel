@extends('layouts.user.app')

@section('content')
<div class="container">
      <div class="card mb-3 mb-5">
          <div class="card-body">
            <form class="" action="/completeAllData" method="post" enctype="multipart/form-data">
              {{csrf_field()}}
                @foreach($user as $users)
              <p>Lengkapi data dibawah ini untuk melamar</p>
              <div class="card-body mb-5" style="background:lightgray;">
                <table>
                  <tr>
                    <td>
                      <h5>Lampiran CV/Resume</h5>
                      @foreach($lowongan as $lowongans)
                      <input type="hidden" class="form-control" name="id_pekerjaan"  value="{{$lowongans->id}}">
                      @endforeach
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5 >Nama </h5>
                    </td>
                    <td> <h5>:</h5> </td>
                    <td>
                      <h5 style="margin-right:350px;"> {{$users->nama_lengkap}}</h5>
                    </td>
                    <td>
                      <input type="file" name="cvorresume" accept="application/pdf" required>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <h5>No Telepon</h5>
                    </td>
                    <td> <h5>:</h5> </td>
                    <td>
                      <h5> {{$users->nomor_telepon}}</h5>
                    </td>
                    <td style="font-weight:bold;">*file yang dimasukan berupa pdf</td>
                  </tr>
                </table>
              </div>
                <h2>Data Pendidikan</h2>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_sd" placeholder="Nama Sekolah SD/Sederajat" value="{{ old('name') }}" required autofocus>
                        <input type="hidden" class="form-control" name="tingkat_pendidikan_sd"  value="SD">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_sd" placeholder="Lama Pendidikan... mis:2001-2007" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_smp" placeholder="Nama Sekolah SMP/Sederajat" value="{{ old('name') }}" required autofocus>
                        <input type="hidden" class="form-control" name="tingkat_pendidikan_smp"  value="SMP">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_smp" placeholder="Lama Pendidikan... mis:2001-2004" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_sma" placeholder="Nama Sekolah SMA/Sederajat" value="{{ old('name') }}" required autofocus>
                        <input type="hidden" class="form-control" name="tingkat_pendidikan_sma"  value="SMA">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_sma" placeholder="Lama Pendidikan... mis:2001-2004" value="{{ old('name') }}" required autofocus>
                    </div>
                    <div class="col-md-4">
                      <select id="jurusan" name="jurusan" class="form-control" required>
                        <option value="IPA">IPA</option>
                        <option value="IPS">IPS</option>
                        <option value="SMK">SMK</option>
                    </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <div style="font-weight:bold;margin-left:20px;">*data dibawah untuk yang sudah melalui jenjang perkuliahan (Jika belum melalui jenjang ini field dapat dikosongkang)</div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_sekolah_lanjutan1" placeholder="Nama Sekolah Universitas/Institusi" value="{{ old('name') }}"autofocus>
                        <input type="hidden" class="form-control" name="tingkat_pendidikan_lanjutan1"  value="Pendidikan Lanjutan">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_lanjutan1" placeholder="Lama Pendidikan... mis:2001-2004" value="{{ old('name') }}" autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="jurusan_pen_lanjutan1" placeholder="Jurusan Pendidikan" value="{{ old('name') }}"  autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                      <input type="hidden" class="form-control" name="tingkat_pendidikan_lanjutan2"  value="Pendidikan Lanjutan">
                        <input type="text" class="form-control" name="nama_sekolah_lanjutan2" placeholder="Nama Sekolah Universitas/Institusi" value="{{ old('name') }}" autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_lanjutan2" placeholder="Lama Pendidikan... mis:2001-2004" value="{{ old('name') }}"  autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="jurusan_pen_lanjutan2" placeholder="Jurusan Pendidikan" value="{{ old('name') }}"  autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                      <input type="hidden" class="form-control" name="tingkat_pendidikan_lanjutan3"  value="Pendidikan Lanjutan">
                        <input type="text" class="form-control" name="nama_sekolah_lanjutan3" placeholder="Nama Sekolah Universitas/Institusi" value="{{ old('name') }}"  autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_pen_lanjutan3" placeholder="Lama Pendidikan... mis:2001-2004" value="{{ old('name') }}"  autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="jurusan_pen_lanjutan3" placeholder="Jurusan Pendidikan" value="{{ old('name') }}"  autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div style="font-weight:bold;margin-left:20px;">*Silahkan "kosongakan" jika anda belum memiliki pengalaman bekerja</div>
                </div>
                <h2>Data Pengalaman Berkerja</h2>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_perusahaan" placeholder="Nama Perusahaan" value="{{ old('name') }}"  autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nama_atasan" placeholder="Nama Atasan" value="{{ old('name') }}" autofocus>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="lama_berkerja" placeholder="Lama Bekerja dalam hitungan bulan" value="{{ old('name') }}"  autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <input type="number" min="1" class="form-control" name="gaji" placeholder="Gaji Pokok" value="{{ old('name') }}"  autofocus>
                    </div>
                    <div class="col-md">
                        <input type="text" class="form-control" name="posisi_terakhir" placeholder="Posisi Terakhir" value="{{ old('name') }}"  autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-12">
                        <textarea name="uraian_pekerjaan" class="form-control" rows="4" cols="80" placeholder="Uraian Pekerjaan"></textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-12">
                        <textarea name="alasan" class="form-control" rows="4" cols="80" placeholder="Alasan Keluar"></textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <h5>Gaji tunjangan yang diinginkan: </h5>
                    </div>
                    <div class="col-md">
                        <input type="number" class="form-control" name="gaji_diinginkan" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <h5>Kapan akan mulai Bekerja: </h5>
                    </div>
                    <div class="col-md">
                        <input type="date" class="form-control" name="mulai_bekerja" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <h5>Nama wali yang dapat dihubungi: </h5>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="nama_wali" value="{{ old('name') }}" required autofocus>
                    </div>
                    <div class="col-md-1">
                        <h5>No hp: </h5>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="no_hp_wali" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>
                <div class="row mb-3">
                  <div class="col-md-3"></div>
                  <div class="col-md-3"></div>
                  <div class="col-md-4"></div>
                  <button class="btn btn-primary col-md-2 mt-3" type="submit" name="button" style="height:60px;">Simpan</button>
                </div>
                @endforeach
            </form>
        </div>
    </div>
</div>
@endsection
