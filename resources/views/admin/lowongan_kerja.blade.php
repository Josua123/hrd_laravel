@extends('layouts.admin.app')

@section('content')
<div class="container">
  <div class="">
      <a href="/admin/lowongan/tambah" class="btn btn-success">Tambah</a>
  </div>
  <hr>
  <div class="row mb-3">
    <div class="col-md-1"></div>
    <div class="col-md-2">Nama Pekerjaan</div>
    <div class="col-md-4">Deskripsi Pekerjaan</div>
    <div class="col-md-2">Tersedia Sampai</div>
    <div class="col-md-1">Status</div>
  </div>
  <hr>
  <?php $i = 1; ?>

  @foreach($lowongan as $lowongans)
    <div class="row mb-3">
      <div class="col-md-1">{{$i}}</div>
      <div class="col-md-2">{{$lowongans->namaPekerjaan}}</div>
      <div class="col-md-4">{{$lowongans->deskripsiPekerjaan}}</div>
      <div class="col-md-2">{{$lowongans->tanggal_expired}}</div>
      <div class="col-md-1">{{$lowongans->status}}</div>
      <div class="col-md-1">
        <a href="{{ route('admin.loker.update.show',$lowongans->id)}}" class="btn btn-primary">Detail</a>
      </div>
      <div class="col-md-1">
        <a href="{{ route('admin.loker.hapus',$lowongans->id)}}" class="btn btn-danger">Hapus</a>
      </div>
    </div>
    <?php $i++; ?>
  @endforeach

</div>
@endsection
