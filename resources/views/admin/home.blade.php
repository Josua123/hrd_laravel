@extends('layouts.admin.app')

@section('content')
<div class="container">
  <hr>
  <div class="row mb-3">
    <div class="col-md-1"></div>
    <div class="col-md-3">Nama Pelamar</div>
    <div class="col-md-3">Pekerjaan yang dilamar</div>
    <div class="col-md-1">Status</div>
  </div>
  <hr>
  <?php
    $i = 1;
  ?>
  @foreach($recruitment as $recruitments)
    <?php
      $user = App\User::where('id',$recruitments->Pelamar_id)->get();
      $lowongan = App\LowonganKerja::where('id',$recruitments->Lowongan_Kerja_id)->get();
    ?>

  <div class="row mb-3">
    <div class="col-md-1">{{$i}}</div>
    @foreach($user as $users)
      <div class="col-md-3">{{$users->nama_lengkap}}</div>
    @endforeach

    @foreach($lowongan as $lowongans)
      <div class="col-md-3">{{$lowongans->namaPekerjaan}}</div>
    @endforeach

    <div class="col-md-2">{{$recruitments->status}}</div>
    @if($recruitments->status == "menunggu")
      <div class="col-md-1">
        <a href="#" class="btn btn-primary">Detail</a>
      </div>
      <div class="col-md-1">
        <a href="#" class="btn btn-success">Terima</a>
      </div>
      <div class="col-md-1">
        <a href="{{ route('tolak.pelamar',$recruitments->id)}}" class="btn btn-danger">Tolak</a>
      </div>
    @endif
  </div>
  <?php
    $i++;
  ?>
  @endforeach

</div>
@endsection
