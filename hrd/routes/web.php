<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/','LowonganController@check')->name('check');
Route::get('/home','LowonganController@show')->name('lowongan.show');
Route::get('/lowongan/{id}','LowonganController@detail')->name('lowongan.detail');
Route::get('/lowongan/{id}/lamar','LamarController@lamar')->name('lowongan.lamar');
Route::get('/search','LowonganController@search');
Route::get('/formlamaran', function () {
    return view('user.form_lamaran_kerja');
})->name('lamaran.submit');
Route::post('/formlaraman/submit','LamarController@submit');
Route::get('/completeAllData','UserController@index')->name('complete.index');
Route::post('/completeAllData','UserController@add');

// Semua yang berhubungan dengan admin akan dimasukkan kedalam Route Group di bawah ini
Route::group(['prefix' => 'admin', 'middleware' => ['admin','auth']],function(){
    Route::get('/','AdminHomeController@check')->name('admin.check');
    Route::get('/home','AdminHomeController@home')->name('admin.home');

    //Lowongan LowonganKerja
    Route::get('/lowongan','AdminLowonganController@show')->name('admin.loker');

    Route::get('/lowongan/tambah', function () {
        return view('admin.create_loker');
    });
    Route::get('/lowongan/detail/{id}','AdminLowonganController@updateShow')->name('admin.loker.update.show');
    Route::post('/lowongan/update/{id}','AdminLowonganController@updateProcess')->name('admin.loker.update.process');
    Route::get('/lowongan/hapus/{id}','AdminLowonganController@delete')->name('admin.loker.hapus');
    Route::post('/tambahlowongan/add','LowonganController@tambah')->name('tambah_lowongan');

    Route::get('/pelamar/{id}/tolak','AdminLamarController@reject')->name('tolak.pelamar');
    Route::get('/pelamar/{id}/terima','AdminLamarController@accept')->name('terima.pelamar');
});

// Kalau untuk yang user disini biar lebih terstruktur aja ya :)
