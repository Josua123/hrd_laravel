@extends('layouts.admin.app')

@section('content')
<div class="container">
@foreach($lowongan as $lowongans)
    <form action="{{route('admin.loker.update.process',$lowongans->id)}}" method="post">
      {{ csrf_field() }}
        <div class="form-group row">
            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pekerjaan') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="nama" value="{{$lowongans->namaPekerjaan}}" required autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="kualifikasi" class="col-md-4 col-form-label text-md-right">{{ __('Kualifikasi Pekerjaan') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="kualifikasi" value="{{$lowongans->kualifikasiPekerjaan}}" required autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi Pekerjaan') }}</label>

            <div class="col-md-6">
                <textarea name="deskripsi" class="form-control" rows="8" cols="74">{{$lowongans->deskripsiPekerjaan}}</textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="bidang" class="col-md-4 col-form-label text-md-right">{{ __('Bidang Pekerjaan') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="bidang" value="{{$lowongans->bidang_pekerjaan}}" required autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="pendidikan" class="col-md-4 col-form-label text-md-right">{{ __('Tingkat Pendidikan') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="pendidikan" value="{{$lowongans->tingkat_pendidikan}}" required autofocus>
            </div>
        </div>

        <div class="form-group row">
            <label for="expired" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Expired') }}</label>

            <div class="col-md-6">
                <input type="date" name="expired" class="form-control" value="{{$lowongans->tanggal_expired->format('Y-m-d')}}">
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Update') }}
                </button>
            </div>
        </div>
    </form>
  @endforeach
</div>
@endsection
