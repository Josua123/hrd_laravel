@extends('layouts.admin.app')

@section('content')
<div class="container">
    <form action="{{route('tambah_lowongan')}}" method="post">
      {{ csrf_field() }}

      <div class="form-group row">
          <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pekerjaan') }}</label>

          <div class="col-md-6">
              <input type="text" class="form-control" name="nama" value="{{ old('name') }}" required autofocus>
          </div>
      </div>

      <div class="form-group row">
          <label for="kualifikasi" class="col-md-4 col-form-label text-md-right">{{ __('Kualifikasi Pekerjaan') }}</label>

          <div class="col-md-6">
              <input type="text" class="form-control" name="kualifikasi" value="{{ old('name') }}" required autofocus>
          </div>
      </div>

      <div class="form-group row">
          <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi Pekerjaan') }}</label>

          <div class="col-md-6">
              <textarea name="deskripsi" class="form-control" rows="8" cols="74"></textarea>
          </div>
      </div>

      <div class="form-group row">
          <label for="bidang" class="col-md-4 col-form-label text-md-right">{{ __('Bidang Pekerjaan') }}</label>

          <div class="col-md-6">
              <input type="text" class="form-control" name="bidang" value="{{ old('name') }}" required autofocus>
          </div>
      </div>

      <div class="form-group row">
          <label for="pendidikan" class="col-md-4 col-form-label text-md-right">{{ __('Tingkat Pendidikan') }}</label>

          <div class="col-md-6">
              <input type="text" class="form-control" name="pendidikan" value="{{ old('name') }}" required autofocus>
          </div>
      </div>

      <div class="form-group row">
          <label for="expired" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Expired') }}</label>

          <div class="col-md-6">
              <input type="date" name="expired" class="form-control" value="">
          </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Tambah') }}
              </button>
          </div>
      </div>
    </form>
</div>
@endsection
