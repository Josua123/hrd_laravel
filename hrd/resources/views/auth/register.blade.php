@extends('layouts.footerRegister')


@section('content')
<div class="container">
  <script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
  </script>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: #8aa8ec; color: #fff; font-size: 20px;" >{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div>
                          <center><h5>Profil Pribadi</h5></center>
                          <hr>
                        </div>

                        <div class="form-group row">
                            <label for="nama" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-6">
                                <input id="nama" type="text" class="form-control" name="nama" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="foto" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>

                            <div class="col-md-6">
                                <input  type="file" name="foto" accept="image/*" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jk" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Kelamin') }}</label>

                            <div class="col-md-6">
                              <label class="radio-inline col-md-3 mt-1 pt-1"> <input type="radio" name="jk" id="pria" value="pria" required>{{ __('Pria') }}</label>
                              <label class="radio-inline"> <input type="radio" name="jk" id="perempuan" value="perempuan">{{ __('Perempuan') }}</label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

                            <div class="col-md-6">
                                <input id="alamat" class="form-control" type="text"  name="alamat" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kota" class="col-md-4 col-form-label text-md-right">{{ __('Kota') }}</label>

                            <div class="col-md-6">
                                <input id="kota" type="text"  class="form-control" name="kota" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kode_pos" class="col-md-4 col-form-label text-md-right">{{ __('Kode Pos') }}</label>

                            <div class="col-md-6">
                                <input id="kode_pos" type="text" class="form-control" name="kode_pos" onkeypress="return hanyaAngka(event)" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_telepon" class="col-md-4 col-form-label text-md-right">{{ __('No Telepon') }}</label>

                            <div class="col-md-6">
                                <input id="no_telepon" type="text" class="form-control" name="no_telepon" onkeypress="return hanyaAngka(event)" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kewarganegaraan" class="col-md-4 col-form-label text-md-right">{{ __('Kewarganegaraan') }}</label>

                            <div class="col-md-6">
                                <input id="kewarganegaraan" type="text" class="form-control" name="kewarganegaraan" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="agama" class="col-md-4 col-form-label text-md-right">{{ __('Agama') }}</label>

                            <div class="col-md-6">
                              <select id="agama" name="agama" class="form-control" required>
                                <option>--Pilih Agama--</option>
                                <option value="Islam">Islam</option>
                                <option value="Protestan">Protestan</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Kong Hu Cu">Kong Hu Cu</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                              <select id="status" name="status" class="form-control">
                                <option>--Pilih Status--</option>
                                <option value="Menikah">Menikah</option>
                                <option value="Belum Menikah">Belum Menikah</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div>
                          <hr>
                          <center><h5>Profil Keluarga</h5></center>
                          <hr>
                        </div>

                        <div class="form-group row">
                            <label for="namaAyah" class="col-md-4 col-form-label text-md-right">{{ __('Nama Ayah') }}</label>

                            <div class="col-md-6">
                                <input id="nama_ayah" type="text" class="form-control" name="namaAyah" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="namaIbu" class="col-md-4 col-form-label text-md-right">{{ __('Nama Ibu') }}</label>

                            <div class="col-md-6">
                                <input id="nama_ibu" type="text" class="form-control" name="namaIbu" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="namaSuamiIstri" class="col-md-4 col-form-label text-md-right">{{ __('Nama Suami/Istri') }}</label>

                            <div class="col-md-6">
                                <input id="nama_suamiIstri" type="text" class="form-control" name="namaSuamiIstri" placeholder="Isi jika ada" value="-" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jumlah_anak" class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Anak') }}</label>
                            <div class="col-md-6">
                                <input id="jumlah_anak" type="number" class="form-control" min="0" value="0" name="jumlah_anak" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jumlah_saudara" class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Saudara') }}</label>
                            <div class="col-md-6">
                                <input id="jumlah_saudara" type="number" class="form-control" min="0" value="0" name="jumlah_saudara" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
