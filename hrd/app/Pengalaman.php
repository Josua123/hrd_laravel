<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengalaman extends Model
{
  protected $table = 'data_pengalaman';
  protected $primaryKey = 'id';
  protected $foreignKey = 'Pelamar_id';

  public $timestamps = false;

  public function user(){
    return $this->belongsTo(User::class);
  }
}
