<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPendidikan extends Model
{
  protected $table = 'detail_tingkatpendidikan';
  protected $primaryKey = 'id';

  public $timestamps = false;

  public function tingkatPendidikan(){
    return $this->hasOne(TingkatPendidikan::class);
  }
}
