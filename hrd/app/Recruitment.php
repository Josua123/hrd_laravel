<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruitment extends Model
{
    //
    protected $table = 'recruitment';
    protected $primaryKey = 'id';
    protected $foreignKey = ['Pelamar_id', 'Lowongan_Kerja_id'];

    public $timestamps = false;

    public function user(){
      return $this->belongsTo(User::class);
    }

    public function lowongankerja(){
      return $this->belongsTo(LowonganKerja::class);
    }

}
