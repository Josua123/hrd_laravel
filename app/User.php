<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_lengkap', 'email', 'password','jenis_kelamin','alamat','kota',
        'kodepos','nomor_telepon','kewarganegaraan','agama','status','jumlah_anak',
        'Role_role_id','foto','status_data',
    ];

    protected $table = 'pelamar';


    protected $foreignKey = 'Role_role_id';

    //tiadakan tabel timestamps
    public $timestamps = false;

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function keluarga(){
        return $this->hasOne(Keluarga::class);
    }

    public function pendidikan(){
        return $this->hasMany(Pendidikan::class);
    }

    public function pengalaman(){
        return $this->hasOne(Pengalaman::class);
    }

    public function isAdmin(){
        if($this->Role_role_id === 1){
          return $this->Role_role_id;
        }
    }

    public function recruitment(){
      return $this->hasMany(Recruitment::class);
    }

    public function notifikasi(){
        return $this->hasMany(Notifikasi::class);
    }
}
