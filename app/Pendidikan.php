<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
  protected $table = 'data_pendidikan';
  protected $primaryKey = 'id';
  protected $foreignKey = ['Pelamar_idPelamar', 'Tingkat_pendidikan_idTingkat_pendidikan'];

  public $timestamps = false;

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function tingkatpendidikan(){
    return $this->belongsTo(TingkatPendidikan::class);
  }
}
