<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    protected $table = 'data_keluarga';
    protected $primaryKey = 'id';
    protected $foreignKey = 'Pelamar_id';

    public $timestamps = false;

    public function user(){
      return $this->belongsTo(User::class);
    }
}
