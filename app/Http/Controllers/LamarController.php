<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request2;
use Auth;
use App\LowonganKerja;
use App\User;
use App\Pendidikan;
use App\Pengalaman;
use App\DetailPendidikan;
use App\TingkatPendidikan;
use App\Recruitment;

class LamarController extends Controller
{
    //harus login baru bisa mengakses halaman.
    public function __construct()
   {
       $this->middleware('auth');
   }

   public function lamar($id){
     $id_user = Auth::user()->id;
     $status_data = Auth::user()->status_data;
     $user = User::where('id',$id_user)->get();
     $lowongan = LowonganKerja::where('id',$id)->get();
     // return view('user.form_lamaran_kerja',compact('lowongan','user'));
     if($status_data == 1){
     return view('user.completeAllData', compact('user','lowongan'));
     }
     else{
       $pendidikans = Pendidikan::where('Pelamar_idPelamar',$id_user)->get();
       $pengalamans = Pengalaman::where('Pelamar_id',$id_user)->get();
       return view('user.form_lamaran_kerja',compact('lowongan','user','pendidikans','pengalamans'));
     }
   }

   public function submit(Request2 $request){

      $user = Auth::user();
      $rekrut = new Recruitment;
      $rekrut->Pelamar_id = $user->id;
      $rekrut->Lowongan_Kerja_id = $request->id_lowongan;
      $rekrut->status = 'menunggu';
      $rekrut->save();


   }
}
