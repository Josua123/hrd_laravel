<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
use App\Notifikasi;
use App\LowonganKerja;
class AdminLamarController extends Controller
{
    //

    function reject($id){
      $recruitments = Recruitment::where('id',$id)->get();

      foreach ($recruitments as $recruitment) {
        // code...
        $lowongans = LowonganKerja::where('id',$recruitment->Lowongan_Kerja_id)->get();


        foreach ($lowongans as $lowongan) {
          // code...
          $notifikasi = new Notifikasi;
          $notifikasi->judul = $lowongan->namaPekerjaan;
          $notifikasi->keterangan = 'Maaf kamu gagal saat kualifikasi';
          $notifikasi->read = '0';
          $notifikasi->pelamar_id = $recruitment->Pelamar_id;
          $notifikasi->save();
        }

        $recruitment->status = 'ditolak';
        $recruitment->save();
      }

      return redirect()->route('admin.home');
    }

    function accept($id){

    }
}
