<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LowonganKerja;
use Auth;
use App\User;
use App\Pendidikan;
use App\Pengalaman;
use App\DetailPendidikan;
use App\TingkatPendidikan;
use App\Recruitment;
class UserController extends Controller
{
    public function index(){
      return view('user.completeAllData');
    }

    public function add(Request $request){
      //memasukan file yang telah diupload ke folder resume
      if($request->hasFile('cvorresume')){
         $destination = "resume";
         $resume = $request->file('cvorresume');
         $filename = $resume->getClientOriginalName();
         $resume->move($destination, $resume->getClientOriginalName());
       }

       //User
       $user = Auth::user();
       $user->resume = $request->cvorresume;
       $user->gajiTunjangan = $request->gaji_diinginkan;
       $user->namaWali = $request->nama_wali;
       $user->noTelpWali = $request->no_hp_wali;
       $user->mulaiBekerja = $request->mulai_bekerja;
       $user->status_data = "2";
       $user->save();

       //Pendidikan
       //SD
       $detail_pendidikan = new DetailPendidikan;
       $detail_pendidikan->nama_sekolah = $request->nama_sd;
       $detail_pendidikan->tahun_pendidikan = $request->lama_pen_sd;
       $detail_pendidikan->jurusan = "-";
       $detail_pendidikan->save();
       $id_sd = $detail_pendidikan->id;
       $tingkat_pendidikan = new TingkatPendidikan;
       $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_sd;
       $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_sd;
       $tingkat_pendidikan->save();
       $id_ting_sd = $tingkat_pendidikan->id;
       $pendidikan = new Pendidikan;
       $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_sd;
       $pendidikan->Pelamar_idPelamar = $user->id;
       $pendidikan->save();
       //SMP
       $detail_pendidikan = new DetailPendidikan;
       $detail_pendidikan->nama_sekolah = $request->nama_smp;
       $detail_pendidikan->tahun_pendidikan = $request->lama_pen_smp;
       $detail_pendidikan->jurusan = "-";
       $detail_pendidikan->save();
       $id_smp = $detail_pendidikan->id;
       $tingkat_pendidikan = new TingkatPendidikan;
       $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_smp;
       $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_smp;
       $tingkat_pendidikan->save();
       $id_ting_smp = $tingkat_pendidikan->id;
       $pendidikan = new Pendidikan;
       $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_smp;
       $pendidikan->Pelamar_idPelamar = $user->id;
       $pendidikan->save();
       //SMA
       $detail_pendidikan = new DetailPendidikan;
       $detail_pendidikan->nama_sekolah = $request->nama_sma;
       $detail_pendidikan->tahun_pendidikan = $request->lama_pen_sma;
       $detail_pendidikan->jurusan = $request->jurusan;
       $detail_pendidikan->save();
       $id_sma = $detail_pendidikan->id;
       $tingkat_pendidikan = new TingkatPendidikan;
       $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_sma;
       $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_sma;
       $tingkat_pendidikan->save();
       $id_ting_sma = $tingkat_pendidikan->id;
       $pendidikan = new Pendidikan;
       $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_sma;
       $pendidikan->Pelamar_idPelamar = $user->id;
       $pendidikan->save();
       //Pendidikan Lanjutan 1
       if($request->nama_sekolah_lanjutan1 != null){
         $detail_pendidikan = new DetailPendidikan;
         $detail_pendidikan->nama_sekolah = $request->nama_sekolah_lanjutan1;
         $detail_pendidikan->tahun_pendidikan = $request->lama_pen_lanjutan1;
         $detail_pendidikan->jurusan = $request->jurusan_pen_lanjutan1;
         $detail_pendidikan->save();
         $id_l1 = $detail_pendidikan->id;
         $tingkat_pendidikan = new TingkatPendidikan;
         $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_lanjutan1;
         $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_l1;
         $tingkat_pendidikan->save();
         $id_ting_l1 = $tingkat_pendidikan->id;
         $pendidikan = new Pendidikan;
         $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_l1;
         $pendidikan->Pelamar_idPelamar = $user->id;
         $pendidikan->save();
       }
       //Pendidikan Lanjutan 2
       if($request->nama_sekolah_lanjutan2 != null){
         $detail_pendidikan = new DetailPendidikan;
         $detail_pendidikan->nama_sekolah = $request->nama_sekolah_lanjutan2;
         $detail_pendidikan->tahun_pendidikan = $request->lama_pen_lanjutan2;
         $detail_pendidikan->jurusan = $request->jurusan_pen_lanjutan2;
         $detail_pendidikan->save();
         $id_l2 = $detail_pendidikan->id;
         $tingkat_pendidikan = new TingkatPendidikan;
         $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_lanjutan2;
         $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_l2;
         $tingkat_pendidikan->save();
         $id_ting_l2 = $tingkat_pendidikan->id;
         $pendidikan = new Pendidikan;
         $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_l2;
         $pendidikan->Pelamar_idPelamar = $user->id;
         $pendidikan->save();
       }
       //Pendidikan Lanjutan 3
       if($request->nama_sekolah_lanjutan3 != null){
         $detail_pendidikan = new DetailPendidikan;
         $detail_pendidikan->nama_sekolah = $request->nama_sekolah_lanjutan3;
         $detail_pendidikan->tahun_pendidikan = $request->lama_pen_lanjutan3;
         $detail_pendidikan->jurusan = $request->jurusan_pen_lanjutan3;
         $detail_pendidikan->save();
         $id_l3 = $detail_pendidikan->id;
         $tingkat_pendidikan = new TingkatPendidikan;
         $tingkat_pendidikan->tingkat_pendidikan = $request->tingkat_pendidikan_lanjutan3;
         $tingkat_pendidikan->detail_tingkatpendidikan_id = $id_l3;
         $tingkat_pendidikan->save();
         $id_ting_l3 = $tingkat_pendidikan->id;
         $pendidikan = new Pendidikan;
         $pendidikan->Tingkat_pendidikan_idTingkat_pendidikan = $id_ting_l3;
         $pendidikan->Pelamar_idPelamar = $user->id;
         $pendidikan->save();
       }

       //Pengalaman
       if($request->nama_perusahaan != null){
         $pengalaman = new Pengalaman;
         $pengalaman->nama_perusahaan = $request->nama_perusahaan;
         $pengalaman->nama_atasan = $request->nama_atasan;
         $pengalaman->rentang_waktu = $request->lama_berkerja;
         $pengalaman->gaji_pokok = $request->gaji;
         $pengalaman->posisi_terakhir = $request->posisi_terakhir;
         $pengalaman->alasan_keluar = $request->alasan;
         $pengalaman->uraian_pekerjaan = $request->uraian_pekerjaan;
         $pengalaman->Pelamar_id = $user->id;
         $pengalaman->save();
       }

       // $rekrut = new Recruitment;
       // $rekrut->Pelamar_id = $user->id;
       // $rekrut->Lowongan_Kerja_id = $request->id_pekerjaan;
       // $rekrut->status = 'menunggu';
       // $rekrut->save();

       $lowongan_id = $request->id_pekerjaan;

       $lowongan = LowonganKerja::where('id',$lowongan_id)->get();
       $pendidikans = Pendidikan::where('Pelamar_idPelamar',$user->id)->get();
       $pengalamans = Pengalaman::where('Pelamar_id',$user->id)->get();


       return redirect()->back();
    }
}
