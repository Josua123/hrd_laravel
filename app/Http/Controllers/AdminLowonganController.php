<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LowonganKerja;

class AdminLowonganController extends Controller
{
  function show(){
      $lowongan = LowonganKerja::all();
      return view('admin.lowongan_kerja',compact('lowongan'));
  }

  function updateShow($id){
    $lowongan = LowonganKerja::where('id',$id)->get();

      return view('admin.update_loker',compact('lowongan'));
  }

  function updateProcess($id,Request $request){
    $lowongan = LowonganKerja::where('id',$id)->first();
    $lowongan->tanggal_expired = $request->expired;
    $lowongan->kualifikasiPekerjaan = $request->kualifikasi;
    $lowongan->deskripsiPekerjaan = $request->deskripsi;
    $lowongan->bidang_pekerjaan = $request->bidang;
    $lowongan->tingkat_pendidikan = $request->pendidikan;
    $lowongan->namaPekerjaan = $request->nama;
    $lowongan->status = "active";
    $lowongan->save();

    return redirect()->route('admin.loker');
  }

  function delete($id){
    $lowongan = LowonganKerja::where('id',$id)->first();
    $lowongan->delete();

    return redirect()->route('admin.loker');
  }
}
