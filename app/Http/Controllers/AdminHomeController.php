<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
class AdminHomeController extends Controller
{
    //
    public function check(){
      return  redirect()->route('admin.home');
    }

    public function home(){
        $recruitment = Recruitment::all();
        return view('admin.home',compact('recruitment'));
    }
}
