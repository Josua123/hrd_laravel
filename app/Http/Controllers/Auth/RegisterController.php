<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Keluarga;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama' => ['required'],
            'jk' => ['required'],
            'alamat' => ['required'],
            'kota' => ['required'],
            'foto' => ['required'],
            'kode_pos' => ['required'],
            'no_telepon' => ['required'],
            'kewarganegaraan' => ['required'],
            'agama' => ['required'],
            'status' => ['required'],
            'jumlah_anak' => ['required'],
            'namaAyah' => ['required'],
            'namaIbu' => ['required'],
            'namaSuamiIstri' => ['required'],
            'jumlah_saudara' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      if(Request::hasFile('foto')){
         $destination = "images\profile";
         $gambar = Request::file('foto');
         $filename = $gambar->getClientOriginalName();
         $gambar->move($destination, $gambar->getClientOriginalName());
       }

        $user = User::create([
            'nama_lengkap' =>$data['nama'],
            'jenis_kelamin' => $data['jk'],
            'alamat' => $data['alamat'],
            'kota' => $data['kota'],
            'kodepos' => $data['kode_pos'],
            'nomor_telepon' => $data['no_telepon'],
            'kewarganegaraan' => $data['kewarganegaraan'],
            'agama' => $data['agama'],
            'status' => $data['status'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'status_data' =>'1',
            'Role_role_id'=> '2',
            'foto' => $gambar->getClientOriginalName(),
        ]);

        $keluarga = new Keluarga;
        $keluarga->nama_ibu = $data['namaIbu'];
        $keluarga->nama_ayah = $data['namaAyah'];
        $keluarga->jumlah_saudara = $data['jumlah_saudara'];
        $keluarga->nama_suami_istri = $data['namaSuamiIstri'];
        $keluarga->jumlah_anak = $data['jumlah_anak'];
        $keluarga->Pelamar_id = $user->id;
        $keluarga->save();

        return $user;


    }
}
