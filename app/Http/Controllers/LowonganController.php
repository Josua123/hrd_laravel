<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LowonganKerja;
use Auth;
class LowonganController extends Controller
{

    function check(){
        $lowongan = LowonganKerja::all();
        foreach ($lowongan as $low) {
            if($low->tanggal_expired >= today()){

            }else {
              $low->status = "expired";
              $low->save();
            }
        }

        return redirect()->route('lowongan.show');
    }

    function show(){
        $status = "active";
        $lowongan = LowonganKerja::where('status',$status)->get();
        return view('user.home',compact('lowongan'));
    }

    function search(Request $request){
      $status = "active";
        $lowongan = LowonganKerja::where('namaPekerjaan', 'like', '%'.$request->search.'%')->where('status',$status)->get();
        return view('user.home',compact('lowongan'));
    }

    function detail($id){
        $lowongan = LowonganKerja::where('id',$id)->get();
        return view('user.detail_lowongan_kerja',compact('lowongan'));
    }


    function tambah(Request $request){
        $lowongan = new LowonganKerja;
        $lowongan->tanggal_expired = $request->expired;
        $lowongan->tanggal_publikasi = today();
        $lowongan->kualifikasiPekerjaan = $request->kualifikasi;
        $lowongan->deskripsiPekerjaan = $request->deskripsi;
        $lowongan->bidang_pekerjaan = $request->bidang;
        $lowongan->tingkat_pendidikan = $request->pendidikan;
        $lowongan->namaPekerjaan = $request->nama;
        $lowongan->status = "active";
        $lowongan->save();

        return view('admin.home');
    }
}
