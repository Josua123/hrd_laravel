<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    //
    protected $table = 'notifikasi';
    protected $primaryKey = 'id';
    protected $foreignKey = ['pelamar_id'];
    public $timestamps = false;

    public function user(){
      return $this->belongsTo(User::class);
    }

}
