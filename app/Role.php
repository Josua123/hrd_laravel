<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $primaryKey = 'role_id';

    public function user(){
        retrurn $this->hasMany(User::class,'Role_role_id','role_id');
    }
}
