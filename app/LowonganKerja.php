<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LowonganKerja extends Model
{
    protected $table = 'lowongan_kerja';
    public $timestamps = false;

    public function recruitment(){
      return $this->hasMany(Recruitment::class);
    }

    protected $dates = ['tanggal_expired'];
}
