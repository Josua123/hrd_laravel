<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TingkatPendidikan extends Model
{
  protected $table = 'tingkat_pendidikan';
  protected $primaryKey = 'id';
  protected $foreignKey = 'detail_tingkatpendidikan_id';

  public $timestamps = false;

  public function detailPendidikan(){
    return $this->belongsTo(DetailPendidikan::class);
  }

  public function pendidikan(){
    return $this->hasOne(Pendidikan::class);
  }
}
